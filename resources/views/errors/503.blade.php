@extends('errors::minimal')

<h2>{{ $exception->getMessage() }}</h2>
@section('title', __('Service Unavailable'))
@section('code', '503')
@section('message', __('Service Unavailable'))
