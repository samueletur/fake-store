<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;

Route::controller(IndexController::class)->group(function () {
    Route::get('', 'index')->name('dashboard');
    Route::get('customers', 'customers')->name('customers');
    Route::get('products', 'products')->name('products');
});
