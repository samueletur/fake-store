<?php

namespace App\Http\Apis;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\PendingRequest;

class CustomersApi extends Api
{
    public function initialize(): PendingRequest
    {
        return Http::withToken($this->token)->acceptJson()->baseUrl(env('STORE_URL'));
    }

    public function all($pageSize = 50, $currentPage = 1, $filters = [])
    {
        return $this->initialize()->get('/customers/search', array_merge($filters, [
            'searchCriteria[pageSize]'    => $pageSize,
            'searchCriteria[currentPage]' => $currentPage,
        ]));
    }
}
