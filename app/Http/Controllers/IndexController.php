<?php

namespace App\Http\Controllers;

use App\Http\Apis\OrdersApi;
use App\Http\Apis\ProductsApi;
use App\Http\Apis\CustomersApi;

class IndexController extends Controller
{
    function index()
    {
        $totalCustomers = (new CustomersApi)->all()->json()['total_count'];
        $totalProducts = (new ProductsApi)->all()->json()['total_count'];

        $orders = (new OrdersApi)->all()->json();
        $totalOrders = collect($orders['items'])->sum('total_paid');

        return view('dashboard', compact([
            'totalCustomers',
            'totalOrders',
            'orders',
            'totalProducts'
        ]));
    }

    function customers()
    {
        return view('customers', [
            'customers' => (new CustomersApi)->all()->json()
        ]);
    }

    function products()
    {
        return view('products', [
            'products' => (new ProductsApi)->all()->json()
        ]);
    }
}
