<?php

namespace App\Http\Apis;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Traits\ForwardsCalls;

abstract class Api
{
    use ForwardsCalls;

    protected PendingRequest $http;

    public function __construct()
    {
        try {
            $this->token = $this->getToken();
            $this->http = $this->initialize();
        } catch (\Throwable $th) {
            abort('500', "A loja está indisponível no momento");
        }
    }

    public function __call($method, $params)
    {
        return $this->forwardCallTo($this->http, $method, $params);
    }

    public function getToken()
    {
        $token = session('token');

        if (empty($token)) {

            $token = Http::acceptJson()->post(env('STORE_URL') . '/integration/admin/token', [
                'username' => env('STORE_USERNAME'),
                'password' => env('STORE_PASSWORD'),
            ])->json();

            session(['token' => $token]);
        }

        return $token;
    }

    public abstract function initialize(): PendingRequest;
}
